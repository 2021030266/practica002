package Controlador;

import modelo.Cotizacion;
import vista.dlgvista;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controlador implements ActionListener {

    private Cotizacion cotizacion;
    private dlgvista vista;

    public Controlador(Cotizacion cotizacion, dlgvista vista) {
        this.cotizacion = cotizacion;
        this.vista = vista;

        // Configurar ActionListener para los botones
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);

    }

    private void iniciarVista() {
        vista.setTitle("::    COTIZACION   ::");
        vista.setSize(700, 500);
        vista.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista.btnLimpiar) {

            vista.txtNumCotización.setText("");
            vista.txtPorcentajeInicial.setText("");
            vista.txtDescripcion.setText("");
            vista.txtPrecio.setText("");
            //vista.BoxPlazo.removeAllItems();

            vista.txtPInicial.setText("");
            vista.txtTfin.setText("");
            vista.txtPMensual.setText("");
            

        }
        if (e.getSource() == vista.btnNuevo) {

            vista.txtNumCotización.setEnabled(true);
            vista.txtPorcentajeInicial.setEnabled(true);
            vista.txtDescripcion.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.BoxPlazo.setEnabled(true);

            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);

        }
        if (e.getSource() == vista.btnGuardar) {
            JFrame Frame = new JFrame("Guardado con exito");
            try {
                cotizacion.setNumCotizacion(Integer.parseInt(vista.txtNumCotización.getText()));
                cotizacion.setDescripcionAutomovil(vista.txtDescripcion.getText());
                
                String precioText = vista.txtPrecio.getText().trim();
                if (!precioText.isEmpty()) {
                    cotizacion.setPrecio(Double.parseDouble(precioText));
                }

                String pInicialText = vista.txtPorcentajeInicial.getText().trim();
                if (!pInicialText.isEmpty()) {
                    cotizacion.setPorcentajePagoInicial(Double.parseDouble(pInicialText));
                }

                if (vista.BoxPlazo.getSelectedItem().toString().length() < 8) {
                    this.cotizacion.setPlazo(Integer.parseInt(this.vista.BoxPlazo.getSelectedItem().toString().substring(0, 1)));
                } else {
                    this.cotizacion.setPlazo(Integer.parseInt(this.vista.BoxPlazo.getSelectedItem().toString().substring(0, 2)));
                }

                JOptionPane.showMessageDialog(vista, "Guardado con Exito");

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: " + ex2.getMessage());
            }
        }

        if (e.getSource() == this.vista.btnMostrar) {
            this.vista.txtNumCotización.setText(String.valueOf(this.cotizacion.getNumCotizacion()));
            this.vista.txtDescripcion.setText(this.cotizacion.getDescripcionAutomovil());
            this.vista.txtPorcentajeInicial.setText(String.valueOf(this.cotizacion.getPorcentajePagoInicial()));
            this.vista.txtPrecio.setText(String.valueOf(this.cotizacion.getPrecio()));
            this.vista.BoxPlazo.getSelectedItem();
            
            this.vista.txtPInicial.setText(String.valueOf(this.cotizacion.Cpagoinicial()));
            this.vista.txtPMensual.setText(String.valueOf(this.cotizacion.PagoMensual()));
            this.vista.txtTfin.setText(String.valueOf(this.cotizacion.Totalfin()));
            
        }
        
        if (e.getSource() == vista.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(vista, "¿Deseas salir?", 
                    "Confirmar salida", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
            if (e.getSource() == vista.btnCancelar) {
                    vista.btnGuardar.setEnabled(false);
                    vista.btnMostrar.setEnabled(false);
                    vista.btnLimpiar.setEnabled(false);
                    vista.btnCancelar.setEnabled(false);
                    vista.txtNumCotización.setEnabled(false);
                    vista.txtDescripcion.setEnabled(false);
                    vista.txtPorcentajeInicial.setEnabled(false);
                    vista.txtPrecio.setEnabled(false);
                    vista.BoxPlazo.setEnabled(false);

                    vista.txtNumCotización.setText("");
                    vista.txtDescripcion.setText("");
                    vista.txtPrecio.setText("");
                    vista.txtPorcentajeInicial.setText("");

                    vista.txtPInicial.setText("");
                    vista.txtPMensual.setText("");
                    vista.txtTfin.setText("");
            }

        }

        public static void main(String[] args) {
            Cotizacion cotizacion = new Cotizacion();
            dlgvista vista = new dlgvista(new JFrame(), true);

            Controlador contra = new Controlador(cotizacion, vista);
            contra.iniciarVista();
        }

        private void Limpiar() {
            vista.txtNumCotización.setText("");
            vista.txtDescripcion.setText("");
            vista.txtPrecio.setText("");
            vista.txtPorcentajeInicial.setText("");
            vista.BoxPlazo.setToolTipText("");
        }
}
